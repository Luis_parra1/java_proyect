
public class VariablesJava {
    public static void main(String[] args) {
        //notacion camel case
        //Definir e inicializar la variable entera
        int miVariableEntera = 10;
        System.out.println(miVariableEntera);

        //Modificar la variable entera
        miVariableEntera = 5;
        System.out.println(miVariableEntera);

        //Definir una variable String
        String miVariableString = "Saludos";
        System.out.println(miVariableString);

        //Modificar varable String
        miVariableString = "Adios";
        System.out.println(miVariableString);

        //Palabra reservada var se usa para inferir el tipo
        //Solo funciona en versiones de jdk 10 o mas
        var miVariableEnteraVar = 10;
        System.out.println(miVariableEnteraVar);

        var miVariableStringVar = "Saludos";
        //atajo soutv
        System.out.println("miVaraibleStringVar = " + miVariableStringVar);

        //Reglas para difinir nombres de variables
        //se usa la primera letra en minuscula y no puede ser un numero o caracteres especiales
        //no se recomienda usar acentos en las palabras
        // _ es un caracter permitido asi como $

        var miVariable = 1;
        var _miVariable = 2;
        var $mivariable = 3;

    }
}