import java.util.Scanner;

public class InformacionLibro {
    public static void main(String[] args) {
        /*
         Se solicita incluir la siguiente información acerca de un libro:

         -titulo

         -autor

         Debes imprimir la información en el siguiente formato:

             Proporciona el titulo:
             Proporciona el autor:
             <titulo> fue escrito por <autor>

         Puedes utilizar el IDE de tu preferencia para desarrollar la solución y después pegar aquí tu respuesta, ya que este editor no contiene ningún tipo de ayuda.

         Preguntas de esta tarea
         1. ¿Cuál es el código del requerimiento solicitado?*/

        Scanner consola = new Scanner(System.in);
        System.out.print("Digite el titulo del libro: ");
        var titulo = consola.nextLine();
        System.out.print("Digite el autor del libro: ");
        var autor = consola.nextLine();

        System.out.println(titulo + " fue escrito por " + autor);

    }
}