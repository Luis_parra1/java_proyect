import java.util.Scanner;

public class ClaseScanner {
    public static void main(String[] args) {
        //Consola es un objeto de tipo Scanner que nos permite obtener lo que se ingresa por consola
        System.out.print("Digite su nombre: ");
        Scanner consola = new Scanner(System.in);

        //Al usar .nextLine() detiene el programa y espera a que se digite algo en la consola
        // luego de presionar enter captura lo que este en la linea de la consola y devuelve un String
        var usuario = consola.nextLine();
        System.out.println("usuario = " + usuario);

        System.out.print("Digite el titulo: ");
        var titulo = consola.nextLine();
        System.out.println("Resultado = " + titulo + " " + usuario);
    }
}