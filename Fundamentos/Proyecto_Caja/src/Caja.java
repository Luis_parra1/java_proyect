public class Caja {

    double ancho;
    double largo;
    double alto;

    public Caja() {
    }

    public Caja(double ancho, double largo, double alto) {
        this.ancho = ancho;
        this.largo = largo;
        this.alto = alto;
    }

    public void calcularVolumen() {
        double volumen = ancho * alto * largo;
        System.out.println("El volumen de la caja es: " + volumen);
    }
}
