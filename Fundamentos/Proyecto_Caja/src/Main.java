import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner consola = new Scanner(System.in);
        Caja caja = new Caja();

        System.out.print("Proporcione el largo: ");
        caja.largo = consola.nextDouble();
        System.out.print("Proporcione el ancho: ");
        caja.ancho = consola.nextDouble();
        System.out.print("Proporcione el alto: ");
        caja.alto = consola.nextDouble();

        caja.calcularVolumen();

    }
}