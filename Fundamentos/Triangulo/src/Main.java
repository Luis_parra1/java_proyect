import java.util.Scanner;

public class Main {
    /*
    En el siguiente ejercicio se solicita calcular el área y el perímetro de un Rectángulo, para ello deberemos crear
    las siguientes variables:
    *alto (int)
    *ancho (int)
    El usuario debe proporcionar los valores de largo y ancho, y después se debe imprimir el resultado en el siguiente
    formato(no usar acentos y respetar los espacios, mayúsculas, minúsculas y saltos de línea):

    *Proporciona el alto:
    *Proporciona el ancho:
    *Area: <area>
    *Perimetro: <perimetro>

    *Las fórmulas para calcular el área y el perímetro de un Rectángulo son:

    Área: alto * ancho

    Perímetro: (alto + ancho) * 2

    Puedes utilizar el IDE de tu preferencia para codificar la solución y después pegar tu solución en esta herramienta.
    * */
    public static void main(String[] args) {
        Scanner consola = new Scanner(System.in);

        int alto;
        int ancho;
        int area;
        int perimetro;

        //Entradas
        System.out.print("Proporciona el alto:");
        alto = consola.nextInt();
        System.out.print("Proporciona el ancho:");
        ancho = consola.nextInt();

        //Proceso
        area = alto * ancho;
        perimetro = (alto + ancho) * 2;

        //Salida
        System.out.println("Area: " + area);
        System.out.println("Perimetro: " + perimetro);
    }
}