public class ConcatenacionJava {
    public static void main(String[] args) {
        var usuario = "Juan";
        var titulo = "Ingeniero";

        var union = titulo + " " + usuario;
        System.out.println("union = " + union);

        var i = 3;
        var j = 4;

        System.out.println(i + j);
        //Out 7

        System.out.println(i + j + usuario);
        //Out 7Juan

        System.out.println(usuario + i + j);
        //Out Juan34 esto sucede porque el contexto empieza con cadena

        System.out.println(usuario + (i + j));
        //Out Juan7 esto sucede porque los parentesis tienen prioridad


    }
}