public class Main {

    /*
    * Operaradores aritmeticos: +, - , *, /, %
    * Operadores de relacion: <, >, <=, >=, !=, ==
    * Operadores logicos; && o &, || o | , !, ^
    * Operadores unitarios: ~, -
    * Operadores a nivel de bits: ++, --, =, *=, /=, %=, +=, -=, <<=, >>=, >>>=, &=, |=, ^=
    * Operador condicional: ?:
    * Prioridad y orden de evaluacion: (), [], ., -~!++--, new (tipo) expresion
    * */


    public static void main(String[] args) {
        //Operadores aritmeticos
        int a = 3, b = 2;
        int resultado;

        //suma (+)
        resultado = a + b;
        System.out.println("resultado suma = " + resultado);

        //resta (-)
        resultado = a - b;
        System.out.println("resultado resta = " + resultado);

        //multiplicacion (*)
        resultado = a * b;
        System.out.println("resultado multiplicacion = " + resultado);

        double resultado2;

        //division (/)
        resultado2 =  (double) a / (double) b;
        System.out.println("resultado division = " + resultado2);

        //modulo (%)
        resultado2 = (double) a / (double) b;
        System.out.println("resultado modulo = " + resultado2);

        // uso de % para averiguar si es par o no
        if (a % 2 == 0) {
            System.out.println("el numero " + a + " es par");
        } else {
            System.out.println("el numero " + a + " es impar");
        }

        if (b % 2 == 0) {
            System.out.println("el numero " + b + " es par");
        } else {
            System.out.println("el numero " + b + " es impar");
        }
        
        //Operador de asignacion
        int A = 3, B = 2;
        int C = A + 5 - B;
        System.out.println("C = " + C);

        //Operadores a nivel de bits

        A++; // A + 1

        A--; // A - 1

        A += 1; //A = A + 1

        System.out.println("A = " + A);

        A += 3; //A = A + 3
        System.out.println("A = " + A);

        A -= 2; // A = A - 2
        System.out.println("A = " + A);

        A *= 3; //A = A * 3
        System.out.println("A = " + A);

        A /= 3; //A = A / 3
        System.out.println("A = " + A);

        A %= 2; //A = A % 2
        System.out.println("A = " + A);

        /* <<= Operador de asignación con desplazamiento a la izquierda.
             Este operador realiza un desplazamiento de bits a la izquierda
             en la variable y asigna el resultado a la misma variable.*/
        A <<= 2; // A = A << 2
        System.out.println("A = " + A);

        /* >>= Operador de asignación con desplazamiento a la derecha.
             Este operador realiza un desplazamiento de bits a la derecha
             en la variable y asigna el resultado a la misma variable.*/
        A >>= 2; // A = A >> 2
        System.out.println("A = " + A);

        /*>>>=: Operador de asignación con desplazamiento a la derecha sin signo.
        Este operador realiza un desplazamiento de bits a la derecha en la variable,
        llenando los bits vacíos con ceros, y asigna el resultado a la misma variable.*/
        A >>>= 2; //A = A >>> 2;
        System.out.println("A = " + A);

        










    }
}