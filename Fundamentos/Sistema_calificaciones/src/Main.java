import java.util.Scanner;

public class Main {
    /*
    El objetivo del ejercicio es crear un sistema de calificaciones, como sigue:

    El usuario proporcionará un valor entre 0 y 10.

    Si está entre 9 y 10: imprimir una A

    Si está entre 8 y menor a 9: imprimir una B

    Si está entre 7 y menor a 8: imprimir una C

    Si está entre 6 y menor a 7: imprimir una D

    Si está entre 0 y menor a 6: imprimir una F

    cualquier otro valor debe imprimir: Valor desconocido

    Por ejemplo:

    Proporciona un valor entre 0 y 10:
    A
    Puedes utilizar el IDE de tu preferencia para codificar la solución y después pegar tu solución en esta herramienta.
    **/
    public static void main(String[] args) {
        Scanner consola = new Scanner(System.in);
        double entrada;

        System.out.print("Proporciona un valor entre 0 y 10: ");
        entrada = consola.nextDouble();

        if (entrada <= 10.0 && entrada >= 9.0) {
            System.out.println("A");
        } else if (entrada < 9.0 && entrada >= 8.0) {
            System.out.println("B");
        } else if (entrada < 8.0 && entrada >= 7.0) {
            System.out.println("C");
        } else if (entrada < 7.0 && entrada >= 6.0) {
            System.out.println("D");
        } else if (entrada < 6.0 && entrada >= 0.0) {
            System.out.println("F");
        } else {
            System.out.println("Valor desconocido");
        }
    }
}