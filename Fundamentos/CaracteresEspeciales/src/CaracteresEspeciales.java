public class CaracteresEspeciales {
    public static void main(String[] args) {
        var nombre = "Karla";

        // \n permite un salto de linea
        System.out.println("Nueva linea: \n" + nombre);

        // \t permite agregar un espacio tipo tabulador
        System.out.println("Tabulador: \t" + nombre);

        // \b retrocede un espacio y borra lo que esta esta en esa posicion
        System.out.println("Retroceso: \b\b" +nombre);

        //agrega una comilla sencilla, se puede usar sin el \.
        System.out.println("Comilla simple: \'" + nombre + "\'");
        System.out.println("Comilla simple: '" + nombre + "'");

        //agrega una comilla doble.
        System.out.println("Comilla simple: \"" + nombre + "\"");

    }
}