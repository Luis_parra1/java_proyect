public class Flotantes {
    public static void main(String[] arg){
        // tipos primitivos de tipo flotante: float y double

        //Float
        float numeroFloat = 3.4028235E38f;

        System.out.println("Valor minimo tipo float: " + Float.MIN_VALUE);
        System.out.println("Valor maximo tipo float: " + Float.MAX_VALUE);

        System.out.println("numeroFloat = " + numeroFloat);

        //Double
        double numeroDouble = 1.7976931348623157E308d;

        System.out.println("Valor minimo tipo double: " + Double.MIN_VALUE);
        System.out.println("Valor maximo tipo double: " + Double.MAX_VALUE);

        System.out.println("numeroDouble = " + numeroDouble);
    }
}
