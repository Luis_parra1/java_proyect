import java.util.Scanner;

@SuppressWarnings("OnlyOneElementUsed")
public class ConversionTiposPrimitivos {
    public static void main(String[] arg){
        var edad = Integer.parseInt("20");
        System.out.println("edad = " + (edad + 1));

        var edadstr = "20";
        System.out.println("edad = " + (edadstr + 1));
        
        var valorPi = Double.parseDouble("3.1416");
        System.out.println("valorPi = " + valorPi);

        //Pedir un valor str y convertirlo a int
        var consola = new Scanner(System.in);
        System.out.println("Proporciona tu edad: ");
        edad = Integer.parseInt( consola.nextLine() );
        System.out.println("edad = " + edad);

        // valueOf acepta multiples tipos
        var edadTxt = String.valueOf(10);
        System.out.println("edadTxt = " + edadTxt);

        // charAt devuelve el indice empieza en 0
        var caracter = "hola".charAt(0);
        System.out.println("caracter = " + caracter);

        //Solicitar un caracter
        caracter = consola.nextLine().charAt(0);
        System.out.println("caracter = " + caracter);
    }
}
