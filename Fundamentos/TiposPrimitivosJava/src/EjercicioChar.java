public class EjercicioChar {
    @SuppressWarnings("UnnecessaryUnicodeEscape")
    public static void main(String[] arg) {
        //aqui se puede asignar cualquier caracter
        char miCaracter = 'a';
        System.out.println("miCaracter = " + miCaracter);

        //unicode es el juego de caracteres mas comun
        char varChar = '\u0021';
        System.out.println("varChar = " + varChar);

        //los caracteres unicode tiene un id decimal por lo que se convierte a ese caracter
        char varCharDecimal = 33;
        System.out.println("varCharDecimal = " + varCharDecimal);

        //Tambien se puede representar con el simbolo
        char varCharSimbolo = '!';
        System.out.println("varCharSimbolo = " + varCharSimbolo);

        //Ejercicio especial con var

        var varChar1 = '\u0021';
        System.out.println("varChar1 = " + varChar1);

        //aqui es obligatorio especificar que es un char porque inferiria un int
        var varCharDecimal1 = (char) 33;
        System.out.println("varCharDecimal1 = " + varCharDecimal1);

        char varCharSimbolo1 = '!';
        System.out.println("varCharSimbolo1 = " + varCharSimbolo1);

        //convertir un char a un int devuleve el numero decimal asociado al simbolo
        int variableEnteraSimbolo = '!';
        System.out.println("variableEnteraSimbolo = " + variableEnteraSimbolo);

    }
}
