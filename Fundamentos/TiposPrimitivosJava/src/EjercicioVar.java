public class EjercicioVar {
    public static void main(String[] arg) {
        //Aqui se puede verificar como se infiere el tipo de dato con var
        var numeroEntero = 10;
        System.out.println("Numero = " + numeroEntero + " es de tipo: " + ((Object) numeroEntero).getClass().getSimpleName());
        var numeroDouble = 10.0;
        System.out.println("Numero = " + numeroDouble + " es de tipo: " + ((Object) numeroDouble).getClass().getSimpleName());
        var numeroFloat = 10.0F;
        System.out.println("Numero = " + numeroFloat + " es de tipo: " + ((Object) numeroFloat).getClass().getSimpleName());
    }
}
