import java.util.Scanner;

@SuppressWarnings("SpellCheckingInspection")
public class EjercicioTiendaLibros {
    public static void main(String[] arg){
        Scanner consola = new Scanner(System.in);
        String nombre;
        int id;
        double precio;
        boolean envioGratuito;

        System.out.println("Proporciona el nombre:");
        nombre = consola.nextLine();
        System.out.println("Proporciona el id:");
        id = Integer.parseInt(consola.nextLine());
        System.out.println("Proporciona el precio:");
        precio = Double.parseDouble(consola.nextLine());
        System.out.println("Proporciona el envio gratuito (true/false):");
        envioGratuito = Boolean.parseBoolean(consola.nextLine());

        System.out.println(nombre + " #" + id);
        System.out.println("Precio: $" + precio);
        System.out.println("Envio Gratuito: " + envioGratuito);

    }
}
