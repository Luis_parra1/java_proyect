public class Enteros {
    public static void main(String[] args) {
        // tipos primitivos enteros: byte, short, int, long

        //Byte
        System.out.println("Valor minimo byte: " + Byte.MIN_VALUE);
        System.out.println("Valor maximo byte: " + Byte.MAX_VALUE);

        byte numeroByte = 127;
        System.out.println(numeroByte);

        //Short
        System.out.println("Valor minimo short: " + Short.MIN_VALUE);
        System.out.println("Valor maximo short: " + Short.MAX_VALUE);

        short numeroShort = 32767;
        System.out.println(numeroShort);

        //Int
        System.out.println("Valor minimo int: " + Integer.MIN_VALUE);
        System.out.println("Valor maximo int: " + Integer.MAX_VALUE);

        int numeroInt = 2147483647;
        System.out.println(numeroInt);

        //Long
        System.out.println("Valor minimo long: " + Long.MIN_VALUE);
        System.out.println("Valor maximo long: " + Long.MAX_VALUE);

        long numeroLong = 9223372036854775807L;
        System.out.println(numeroLong);
    }
}