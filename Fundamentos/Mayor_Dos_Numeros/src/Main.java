import java.util.Scanner;

public class Main {
    /*
    Solicitar al usuario dos valores:

    numero1 (int)

    numero2 (int)

    Se debe imprimir el mayor de los dos números (la salida debe ser identica a la que sigue):

    Proporciona el numero1:
    Proporciona el numero2:
    El numero mayor es:
    <numeroMayor>
    Puedes utilizar el IDE de tu preferencia para codificar la solución y después pegar tu solución en esta herramienta.
    **/
    public static void main(String[] args) {
        Scanner consola = new Scanner(System.in);

        int numero1;
        int numero2;

        //Entradas
        System.out.print("Proporciona el numero1: ");
        numero1 = consola.nextInt();
        System.out.print("Proporciona el numero2: ");
        numero2 = consola.nextInt();

        //Proceso - Salida
        if (numero1 > numero2) {
            System.out.println("El numero mayor es: \n" + numero1);
        } else {
            System.out.println("El numero mayor es: \n" + numero2);
        }
    }
}